# Components

Bulma Components offer specific layouts with lots of possibilities. They include:

* Breadcrumb
* Card
* Dropdown
* Menu
* Message
* Modal
* Navbar
* Pagination
* Panel
* Tabs

Below are some examples from the [documentation](https://bulma.io/documentation/components/).  

Breadcrumb

![breadcrumb](../images/breadcrumb.png)

Card

![card](../images/card.png)

Dropdown

![dropdown](../images/dropdown.png)

Menu

![menu](../images/menu.png)

Message

![message](../images/message.png)

Navbar

![pagination](../images/pagination.png)

Panel

![panel](../images/panel.png)

Tabs

![tabs](../images/tabs.png)

[<---- Layouts](https://gitlab.com/pkrull/academy-bulma/blob/master/lessons/08.Layouts.md)